INSERT INTO `distributors` (`name`, `created_at`)
    SELECT DISTINCT
        `local_distributor` AS `name`,
        NOW() AS `created_at`
    FROM `import_table`
    WHERE `import_table`.`local_distributor` != ''
    UNION DISTINCT
    SELECT DISTINCT
        `usa_distributor` AS `name`,
        NOW() AS `created_at`
    FROM `import_table`
    WHERE `import_table`.`usa_distributor` != ''
ON DUPLICATE KEY UPDATE
    `name` = VALUES(`name`),
    `updated_at` = VALUES(`created_at`);
