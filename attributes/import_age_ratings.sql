INSERT INTO `age_ratings` (`name`, `territory_id`, `created_at`)
    SELECT DISTINCT `age_restriction` AS `name`, `territory_id`, NOW() AS `created_at`
    FROM `movie_performance_data`
ON DUPLICATE KEY UPDATE
    `updated_at` = VALUES(`created_at`);
