DELIMITER $$
DROP PROCEDURE IF EXISTS ImportPerformanceData$$
CREATE PROCEDURE ImportPerformanceData(territory VARCHAR(255), second INT)
BEGIN
    SET @prefix = 'local';
    
    IF second = 1 THEN
        SET @prefix = 'usa';
    END IF;
    
    SET @statement = CONCAT('
    INSERT INTO `movie_performance_data` (
            `movie_id`,
	        `territory_id`,
	        `distributor_id`,
	        `releasedate`,
	        `length`,
	        `title`,
	        `synopsis`,
	        `age_restriction`,
	        `first_weekend_locations`,
	        `first_weekend_number_days`,
	        `boxoffice_first_weekend`,
	        `boxoffice_first_week`,
	        `boxoffice_total`,
	        `admissions_first_weekend`,
	        `admissions_first_week`,
	        `admissions_total`,
	        `created_at`)
        SELECT 
	        `movies`.`id` AS `movie_id`,
	        `territories`.`id` AS `territory_id`,
	        `distributors`.`id` AS `distributor_id`,
	        STR_TO_DATE(`',@prefix,'_releasedate`, \'%d.%m.%Y\') AS `releasedate`,
	        `',@prefix,'_length` AS `length`,
	        `',@prefix,'_title` AS `title`,
	        `',@prefix,'_synopsis` AS `synopsis`,
	        `',@prefix,'_age_restriction` AS `age_restriction`,
	        `',@prefix,'_first_weekend_locations` AS `first_weekend_locations`,
	        `',@prefix,'_first_weekend_number_days` AS `first_weekend_number_days`,
	        `',@prefix,'_boxoffice_first_weekend` AS `boxoffice_first_weekend`,
	        `',@prefix,'_boxoffice_first_week` AS `boxoffice_first_week`,
	        `',@prefix,'_boxoffice_total` AS `boxoffice_total`,
	        `',@prefix,'_admissions_first_weekend` AS `admissions_first_weekend`,
	        `',@prefix,'_admissions_first_week` AS `admissions_first_week`,
	        `',@prefix,'_admissions_total` AS `admissions_total`,
	        NOW() AS `created_at`
        FROM `import_table`
        INNER JOIN `movies`
	        ON `movies`.`rentrak_record_id` = `import_table`.`rentrak_id`
        INNER JOIN `territories`
	        ON `territories`.`name` = \'',TRIM(territory),'\'
	LEFT JOIN `distributors`
	        ON `import_table`.`',@prefix,'_distributor` = `distributors`.`name`
        WHERE `',@prefix,'_releasedate` != \'\'
	    ON DUPLICATE KEY UPDATE
            `updated_at` = VALUES(`created_at`),
            `distributor_id` = VALUES(`distributor_id`),
            `releasedate` = VALUES(`releasedate`),
	        `length` = VALUES(`length`),
	        `title` = VALUES(`title`),
	        `synopsis` = VALUES(`synopsis`),
	        `age_restriction` = VALUES(`age_restriction`),
	        `first_weekend_locations` = VALUES(`first_weekend_locations`),
	        `first_weekend_number_days` = VALUES(`first_weekend_number_days`),
	        `boxoffice_first_weekend` = VALUES(`boxoffice_first_weekend`),
	        `boxoffice_first_week` = VALUES(`boxoffice_first_week`),
	        `boxoffice_total` = VALUES(`boxoffice_total`),
	        `admissions_first_weekend` = VALUES(`admissions_first_weekend`),
	        `admissions_first_week` = VALUES(`admissions_first_week`),
	        `admissions_total` = VALUES(`admissions_total`);');
    PREPARE stmt1 FROM @statement;
    EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;
END$$
DELIMITER ;
SHOW WARNINGS;
