# MetricSwallower How-To

## How to run MetricSwallower

1. place csv files into root folder of MetricSwalloer. (Ensure to not change given name-pattern. The 
2. provided name looks like: *2000 YTD DE-US.csv* containing both territories in the filename.)

1. run MetricSwalloer with
  ```
  ./metric-swallower <password> <db-name>
  ```
  with password of the the mysql-user admin and db-name of the mysql-database


## How to change import columns/parameters

1. See **import_table.sql**. It defines every column type and name of the imported CSV.

1. To change the columns, simply rename, add or remove additional columns

1. For multi-dimensional attributes the columns have to be named with the pattern of *attribute_1*, *attribute_2* and so on.

1. Within **metric-swallower** the call of *ImportFields(attributeName, numOfColumns, tablename)* can be adjusted for every attribute. Example *ImportFields('genre', 3, 'genres) will import columns *genre_1*, *genre_2*, *genre_3* into tables *genres* and *movie_genres* automatically. Note that rails default naming pattern has to be kept in order for metric-swallower to work properly.

1. One exception exists for importing talents, because those use a special-case relation combined with the Talent type. No tablename will be give here, since they all get imported into the *talents* and *movie_talents* tables.

1. Another exception lies within importing the animation attribute. After importing all genres the *movies* tables gets updated by setting animation to true for all movies that have at least one occurrance of the genre 'Animation'. No settings or parameters here. 

# Concept Document

<https://docs.google.com/document/d/1tCBg0TuBDgWSKvi5M77S2MBUCksteqJc7EFmH8f3HKY/edit?usp=sharing>
