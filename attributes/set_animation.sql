UPDATE `movies` SET `animation` = 1 WHERE `id` IN (
    SELECT `movie_id` FROM `movie_genres`
    WHERE `genre_id` IN (
        SELECT `id` FROM `genres` WHERE `name` = "Animation" OR `name` = "CGI"
    )
)
