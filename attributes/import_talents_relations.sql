DELIMITER $$
DROP PROCEDURE IF EXISTS ImportTalentsRelations$$
CREATE PROCEDURE ImportTalentsRelations(talentType VARCHAR(255), maxColumns INT)
BEGIN
    SET @x = 1;
    WHILE @x <= maxColumns DO
        SET @statement = CONCAT('
        INSERT INTO movie_talents (
                `movie_id`,
                `talent_id`, 
                `talent_type_id`, 
                `sort_order`, 
                `created_at`)
            SELECT 
                `movies`.`id` AS `movie_id`,
                `talents`.`id` AS `talent_id`,
                `talent_types`.`id` AS `talent_type_id`,
                ',@x,' AS `sort_order`,
                NOW() AS `created_at`
            FROM `import_table`
            INNER JOIN `talents`
                ON `import_table`.`',TRIM(talentType),'_',@x,'` = `talents`.`name`
            INNER JOIN `movies`
                ON `import_table`.`rentrak_id` = `movies`.`rentrak_record_id`
            INNER JOIN `talent_types`
                ON `talent_types`.`name` = "',TRIM(talentType),'"
            WHERE `import_table`.`',TRIM(talentType),'_',@x,'` != ""
        ON DUPLICATE KEY UPDATE
            `updated_at` = VALUES(`created_at`),
            `sort_order` = VALUES(`sort_order`);');
        PREPARE stmt1 FROM @statement;
        EXECUTE stmt1;
        DEALLOCATE PREPARE stmt1;
        
        SET @x = @x + 1; 
    END WHILE;
END$$
DELIMITER ;
SHOW WARNINGS;

