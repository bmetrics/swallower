DELIMITER $$
DROP PROCEDURE IF EXISTS ImportFieldRelations$$
CREATE PROCEDURE ImportFieldRelations(field VARCHAR(255), maxColumns INT, importTable VARCHAR(255), referenceTable VARCHAR(255))
BEGIN
    SET @x = 1;
    WHILE @x <= maxColumns DO
        SET @statement = CONCAT('
        INSERT INTO `',importTable,'` (
                `movie_id`,
                `',TRIM(field),'_id`,
                `sort_order`, 
                `created_at`)
            SELECT 
                `movies`.`id` AS `movie_id`,
                `',TRIM(referenceTable),'`.`id` AS `',TRIM(field),'_id`,
                ',@x,' AS `sort_order`,
                NOW() AS `created_at`                
            FROM `import_table`
            INNER JOIN `',TRIM(referenceTable),'`
                ON `import_table`.`',TRIM(field),'_',@x,'` = `',TRIM(referenceTable),'`.`name`
            INNER JOIN `movies`
                ON `import_table`.`rentrak_id` = `movies`.`rentrak_record_id`
            WHERE `import_table`.`',TRIM(field),'_',@x,'` != \'\'
        ON DUPLICATE KEY UPDATE
            `updated_at` = VALUES(`created_at`),
            `sort_order` = VALUES(`sort_order`);');
        PREPARE stmt1 FROM @statement;
        EXECUTE stmt1;
        DEALLOCATE PREPARE stmt1;
        
        SET @x = @x + 1; 
    END WHILE;
END$$
DELIMITER ;
SHOW WARNINGS;

