DELIMITER $$
DROP PROCEDURE IF EXISTS ImportFields$$
CREATE PROCEDURE ImportFields(field VARCHAR(255), maxColumns INT, importTable VARCHAR(255))
BEGIN
    SET @x = 1;
    WHILE @x <= maxColumns DO
        SET @statement = CONCAT('
        INSERT INTO `',TRIM(importTable),'` (`name`, `created_at`)
        SELECT DISTINCT
            `',TRIM(field),'_',@x,'` AS `name`,
            NOW() AS `created_at`
        FROM `import_table`
        WHERE `',TRIM(field),'_',@x,'` != \'\'
        ON DUPLICATE KEY UPDATE
            `name` = VALUES(`name`),
            `updated_at` = VALUES(`created_at`);');
        PREPARE stmt1 FROM @statement;
        EXECUTE stmt1;
        DEALLOCATE PREPARE stmt1;
        
        SET @x = @x + 1; 
    END WHILE;
END$$
DELIMITER ;
SHOW WARNINGS;
