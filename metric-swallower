#!/bin/bash

# make sure to abort import in case of errors
set -e

# Echo some basic info
echo "Metric Swallower v0.1"
echo "Author: Stavros Deisavas"
echo ""

# remove previously created log files
printf "Clear logs..."
for file in logs/*.log ; do
    if [ -f "$file" ]
    then
        rm "logs/"$(basename "$file")
    fi
done
echo "ok"

echo ""
echo "Import rentrak csv-files:"
# search for csv-files
for file in ./*.csv ; do
    fileName=$(basename "$file")
    echo ""
    echo "Found Rentrak csv-file: $fileName"
    # get territories
    splitFilename=($(echo $fileName | tr '-' ' ' | tr ' ' "\n"))
    territoryFirst=${splitFilename[2]}
    territorySecond=${splitFilename[3]}
    # Ensure to use ISO2 format only
    if [ $territoryFirst == 'UK' ] ; then
        territoryFirst='GB'
    fi
    if [ $territoryFirst == 'USA' ] ; then
        territoryFirst='US'
    fi
    if [ $territorySecond == 'USA' ] ; then
        territorySecond='US'
    fi
    echo "Detected territores: $territoryFirst and $territorySecond"

    # create import table
    mysql -u root -p$1 $2 < import_table.sql
    
    # import data into mysql
    printf "Import rentrak-file into MySQL..."
    echo ""
    cp "$fileName" import_table
    mysql -u root -p$1 --local-infile $2 < import_csv.sql > logs/import_file.log
    rm import_table
    echo "ok"
    
    # add movies into table
    printf "Add new movies..."
    mysql -u root -p$1 $2 < import_movies.sql > logs/import_movies.log
    echo "ok"
    
    # add all new people
    printf "Add new people..."        
    mysql -u root -p$1 $2 < attributes/import_talents.sql > logs/import_talents.log
    echo "ok"
    
    # import talents relations
    mysql -u root -p$1 $2 < attributes/import_talents_relations.sql > logs/import_talents_relations.log
    printf "Add actors..."
    mysql -u root -p$1 $2 -e "CALL ImportTalentsRelations('Actor',    5);"
    echo "ok"
    printf "Add directors..."
    mysql -u root -p$1 $2 -e "CALL ImportTalentsRelations('Director', 2);"
    echo "ok"
    printf "Add producers..."
    mysql -u root -p$1 $2 -e "CALL ImportTalentsRelations('Producer', 2);"
    echo "ok"
    printf "Add writers..."
    mysql -u root -p$1 $2 -e "CALL ImportTalentsRelations('Writer', 2);"
    echo "ok"
    
    # Update Import Fields Function
    mysql -u root -p$1 $2 < attributes/import_fields.sql > logs/import_fields.log 
    
    printf "Add new languages..."
    mysql -u root -p$1 $2 -e "CALL ImportFields('language', 4, 'languages'); SHOW WARNINGS;" > logs/import_languages.log
    echo "ok"

    printf "Add new keywords..."
    mysql -u root -p$1 $2 -e "CALL ImportFields('keyword', 4, 'keywords'); SHOW WARNINGS;" > logs/import_keywords.log
    echo "ok"

    printf "Add new countries..."
    mysql -u root -p$1 $2 -e "CALL ImportFields('country', 3, 'countries'); SHOW WARNINGS;" > logs/import_keywords.log
    echo "ok"

    printf "Add new genres..."
    mysql -u root -p$1 $2 -e "CALL ImportFields('genre', 3, 'genres'); SHOW WARNINGS;" > logs/import_keywords.log
    echo "ok"

    printf "Set animation movies by Genre 'Animation'..."
    mysql -u root -p$1 $2 < attributes/set_animation.sql > logs/import_fields.log 
    echo "ok"

    # remove Import Fields Function
    mysql -u root -p$1 $2 -e "DROP PROCEDURE IF EXISTS ImportLanguages;"
    
    # Update Import Field Relations Function
    mysql -u root -p$1 $2 < attributes/import_field_relations.sql > logs/import_field_relations.log
    
    printf "Update relations: movie-languages..."
    mysql -u root -p$1 $2 -e "CALL ImportFieldRelations('language', 4, 'movie_languages', 'languages'); SHOW WARNINGS;" > logs/import_language_relations.log
    echo "ok"    

    printf "Update relations: movie-keywords..."
    mysql -u root -p$1 $2 -e "CALL ImportFieldRelations('keyword', 4, 'movie_keywords', 'keywords'); SHOW WARNINGS;" > logs/import_genre_relations.log
    echo "ok"
    
    printf "Update relations: movies-genres..."
    mysql -u root -p$1 $2 -e "CALL ImportFieldRelations('genre', 3, 'movie_genres', 'genres'); SHOW WARNINGS;" > logs/import_genre_relations.log
    echo "ok"    
    
    printf "Update relations: movies-countries..."
    mysql -u root -p$1 $2 -e "CALL ImportFieldRelations('country', 3, 'movie_countries', 'countries'); SHOW WARNINGS;" > logs/import_genre_relations.log
    echo "ok"  
    
    # remove Import Fields Function
    mysql -u root -p$1 $2 -e "DROP PROCEDURE IF EXISTS ImportFieldRelations;"

    printf "Add new distributors..."
    mysql -u root -p$1 $2 < attributes/import_distributors.sql > logs/import_distributors.log
    echo "ok"

    # Update import performance data function
    mysql -u root -p$1 $2 < attributes/import_performance_data.sql > logs/import_performance_data.log
    printf "Add new performance data $territoryFirst..."
    mysql -u root -p$1 $2 -e "CALL ImportPerformanceData('$territoryFirst', 0); SHOW WARNINGS;" > logs/import_performance_data.log
    echo "ok"
    
    printf "Add new performance data $territorySecond..."
    mysql -u root -p$1 $2 -e "CALL ImportPerformanceData('$territorySecond', 1); SHOW WARNINGS;" > logs/import_performance_data.log
    echo "ok"

    printf "Remove temporary import table..."
    mysql -u root -p$1 $2 -e "DROP TABLE IF EXISTS import_table"
    echo "ok"

done

printf "Import all new age ratings..."
mysql -u root -p$1 $2 < attributes/import_age_ratings.sql > logs/import_age_ratings.log
echo "ok"

# send report of logs via mail
printf "Create report mail..."
reportfile="report.mail"
if [ -f "$reportfile" ]
then
    rm "$reportfile"
fi
cat `find logs/*.log` > report.mail
sendmail -f stavros@delisavas.de stavros@delisavas.de < report.mail
echo "ok";

exit 0;
