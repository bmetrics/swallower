INSERT INTO `movies` (`name`, `rentrak_record_id`, `created_at`, `updated_at`)
    SELECT DISTINCT
        `title` AS `name`,
        `rentrak_id` AS `rentrak_record_id`,
        NOW() AS `created_at`,
        NULL AS `updated_at`
    FROM `import_table`
    WHERE `rentrak_id` > 0
ON DUPLICATE KEY UPDATE `name` = VALUES(`name`), `updated_at` = VALUES(`created_at`);
SHOW WARNINGS;
