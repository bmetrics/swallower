DELIMITER $$
DROP PROCEDURE IF EXISTS ImportTalentsByType$$
CREATE PROCEDURE ImportTalentsByType(talentType VARCHAR(255), maxColumns INT)
BEGIN
    SET @x = 1;
    WHILE @x <= maxColumns DO
        SET @statement = CONCAT('
        INSERT INTO talents (`name`, `created_at`)
        SELECT DISTINCT
            `',talentType,'_',@x,'` AS `name`,
            NOW() AS `created_at`
        FROM `import_table`
        ON DUPLICATE KEY UPDATE
            `name` = VALUES(`name`),
            `updated_at` = VALUES(`created_at`);');
        PREPARE stmt1 FROM @statement;
        EXECUTE stmt1;
        DEALLOCATE PREPARE stmt1;
        
        SET @x = @x + 1; 
    END WHILE;
END$$
DELIMITER ;

CALL ImportTalentsByType('Actor', 5);
CALL ImportTalentsByType('Producer', 2);
CALL ImportTalentsByType('Writer', 2);
CALL ImportTalentsByType('Director', 2);
SHOW WARNINGS;
